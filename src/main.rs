extern crate clap;
extern crate dirs;
extern crate tree_magic;

use std::{
	fs::{File},
	io::{Read, Write},
	path::{Path, PathBuf}
};

use clap::{Arg, App};
use dirs::*;
use serde::{Deserialize, Serialize};
use serde_json;

#[derive(Clone,Default,Deserialize,Serialize)]
struct Config {
	last_image: PathBuf,
	image_dir: String,
	image_list: Vec<PathBuf>
}

impl Config {
	fn next_image (&mut self, config_file: &PathBuf) -> std::string::String {
		// Get the current size of the image_list
		let mut length = self.image_list.len();
		// reduce length by one so it matches up with the positions we use later.
		if length != 0 {
			length = length - 1;
		}

		// find the position of the last_image in the image_list vector
		let image_position = self.image_list.iter().position(|r| r == &self.last_image).unwrap();

		// If we're not at the end of the Vector, advance one more position
		if image_position != length {
			let new_index = image_position + 1;
			let new_image = &self.image_list[new_index].as_path();
			self.last_image = new_image.to_path_buf();
			// otherwise wrap round to the beginning again
		} else {
			let new_index: usize = 0;
			let new_image = &self.image_list[new_index].as_path();
			self.last_image = new_image.to_path_buf();
		}
		// save the new location in the list to the config file
		self.save_config(config_file);

		// return the new image for the wallpaper swayidle wants as a String
		let new_image = self.last_image.to_str().unwrap();
		return String::from(new_image);
	}
	fn load_config(self, config_file: &PathBuf) -> Config {
		// Open the config file, or fail if it doesn't exist.
		let mut config_handle = File::open(config_file).expect("Failed to open file for reading");
		// Set aside a buffer to read the file content into
		let mut json_config = String::new();
		// Read the content
		config_handle.read_to_string(&mut json_config).unwrap();
		// Cast to str
		let json_config_as_str = json_config.as_mut_str();
		// Return de-serialized output
		return serde_json::from_str(&json_config_as_str).unwrap();
	}
	fn save_config(&self, config_file: &PathBuf) {
		// Serialize the current content of the Config struct
		let json_config = serde_json::to_string_pretty(&self).unwrap();
		// Open the config file for writing, or fail if we can't
		let mut config_handle = File::create(config_file).expect("Failed to open file for writing");
		// Write the content to the config file, or fail if can't.
		config_handle.write_all(json_config.as_bytes()).expect("Failed writing to file");
	}
	fn build_image_list(&mut self, dir_name: String) {
		// Cast image_dir to str
		let image_dir_as_str :&str = dir_name.as_ref();
		// Create Path from the str
		let image_dir = Path::new(image_dir_as_str);
		// Get a list of all files in the image_dir directory
		let images = std::fs::read_dir(image_dir);
		// loop over each entry
		for entry in images.unwrap() {
			// unwrap to actual value
			let entry = entry.unwrap();
			// get path of individual entry
			let path = entry.path();
			// get mime type of entry content
			let file_type = tree_magic::from_filepath(path.as_path());
			// check to see if the mime type starts with 'image'
			if file_type.starts_with("image") {
				// if it does, convert the entry Path to a PathBuf
				let path_buf = path.to_path_buf();
				// Append this PathBuf to the image_list Vec in the Config
				self.image_list.push(path_buf);
			}
		}
		// Set image_dir in Config to the dir_name
		self.image_dir = dir_name;
		// set last_image in Config to the first entry in the image_list Vec
		self.last_image = self.image_list.first().unwrap().to_path_buf();
	}
}

fn main() {
	let mut config_content: Config = Config::default();
	let config_file;

	// Read command line arguments
	let matches = App::new("swaylock_imagechanger")
		.version("0.1.0")
		.arg(Arg::with_name("init")
		 .short("i")
		 .long("init")
		 .takes_value(false)
		 .requires("imagedir")
		 .help("Whether to initialize the list of images or not"))
		.arg(Arg::with_name("imagedir")
		 .short("d")
		 .long("imagedir")
		 .takes_value(true)
		 .requires("init")
		 .help("The directory containing images to use for swaylock"))
		.get_matches();

	config_file = init_config().unwrap();

	// If --init is supplied on the command line - load all the image files
	// in the --imagedir directory and write them into the config file
	if matches.is_present("init") {
		if let Some(image_dir) = matches.value_of("imagedir") {
			config_content.build_image_list(image_dir.to_string());
			config_content.save_config(&config_file);
		}
	}

	// Load config content
	config_content = config_content.load_config(&config_file);
	let image_to_print = config_content.next_image(&config_file);
	println!("{}",image_to_print);
}

fn init_config() -> Option<std::path::PathBuf> {
	// Find user's config directory
	if let Some(mut config_dir) = config_dir() {
		// Append our app name to the path returned as the config directory
		config_dir.push("rs_imagechanger");

		// Convert PathBuf to Path
		let config_dir_as_path = config_dir.as_path();

		// Check to see if our own config directory exists already
		if !config_dir_as_path.exists() {
			// If it doesn't, create it
			match std::fs::create_dir_all(config_dir_as_path) {
				Err(why) => println!("! {:?}",why.kind()),
				Ok(_) => {}
			}
		} else {
			// if it does - check to see if it's really a directory
			if config_dir_as_path.is_file() {
				// if it's not, exit with an error
				println!("{:?} already exists as a file, instead of a directory",config_dir);
				std::process::exit(1);
			}
		}

		// Now append actual config file name to our config dir path
		config_dir.push("config");

		return Some(config_dir);
	}
	return None;
}
